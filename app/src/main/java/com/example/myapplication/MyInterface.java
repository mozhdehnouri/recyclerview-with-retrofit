package com.example.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public  interface MyInterface {


     String FEED="posts/2/comments";
    @GET(FEED)
    Call<List<User>> getUser();

}
