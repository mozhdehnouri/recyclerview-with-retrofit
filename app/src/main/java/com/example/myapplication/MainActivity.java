package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity {
    private Button btn_sendrequst;
    private RecyAdapter adpter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_sendrequst=findViewById(R.id.btncrt);
        btn_sendrequst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyInterface myInterface=ApiClient.getclient().create(MyInterface.class);
                Call<List<User>>  myuser=myInterface.getUser();
                myuser.enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        List<User> userList = new ArrayList<>();
                        if (response.isSuccessful()) {
                            if (response.body().size() == 0) {
                               Toast.makeText(MainActivity.this, "Erorr", Toast.LENGTH_LONG).show();
                          } else {
                              for (int i = 0; i < response.body().size(); i++) {
                                  userList.add(response.body().get(i));
                              }
                            }
                            Recyadded(userList);
                        }
                    }
                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Erorr",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
    private void Recyadded(List<User> userList) {
        recyclerView=findViewById(R.id.recy1);
        adpter=new RecyAdapter(userList,this);
      linearLayoutManager=new LinearLayoutManager(this);
      recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adpter);
       RecyclerView.ItemDecoration diveder=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
       recyclerView.addItemDecoration(diveder);
        adpter.notifyDataSetChanged();
    }
}
