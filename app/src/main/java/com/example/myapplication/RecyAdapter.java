package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyAdapter extends RecyclerView.Adapter<RecyAdapter.MyViewholder>{
    List<User> mylist;
    Context context;

    public RecyAdapter(List<User> mylist, Context context) {
        this.mylist = mylist;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.items_forrecy,parent,false);

        MyViewholder myholder=new MyViewholder(v);


        return myholder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {
        holder.tid.setText (String.valueOf( mylist.get(position).getId()));
        holder.temail.setText(mylist.get(position).getEmail());
        holder.tname.setText(mylist.get(position).getName());
        holder.tbody.setText(mylist.get(position).getBody().substring(0,40)+"...");
    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {

    TextView tname,tid,temail,tbody;
        public MyViewholder(@NonNull View itemView) {
            super(itemView);
            tname=itemView.findViewById(R.id.txtname);
            tid=itemView.findViewById(R.id.txtid);
            temail=itemView.findViewById(R.id.txtemail);
            tbody=itemView.findViewById(R.id.txtbody);
        }
    }
}
